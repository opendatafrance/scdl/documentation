#!/usr/bin/env python3


"""
Generate files (schemas, XLSX templates and SUMMARY.md) from a catalog of schemas.
"""


import argparse
import itertools
import logging
import subprocess
import sys
import unicodedata
from pathlib import Path
from urllib.parse import urljoin

import requests
from jinja2 import Template

from opendataschema import GitSchemaReference, SchemaCatalog, by_commit_date

log = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('catalog_path_or_url', help='URL of catalog file')
    parser.add_argument('schemas', type=Path, help='schemas output directory')
    parser.add_argument('templates', type=Path, help='templates output directory')
    parser.add_argument('summary_j2', type=Path, help='Jinja2 template for SUMMARY.md')
    parser.add_argument('summary_md', type=Path, help='output file path for SUMMARY.md')
    parser.add_argument('--log', default='WARNING', help='level of logging messages')
    parser.add_argument('--ref', help='Git reference to try to load with Git schema references')
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(message)s",
        level=numeric_level,
        stream=sys.stderr,  # script outputs data
    )

    if not args.schemas.is_dir():
        parser.error("schemas directory does not exist")
    if not args.templates.is_dir():
        parser.error("templates directory does not exist")
    if not args.summary_j2.is_file():
        parser.error("summary_j2 file does not exist")
    if not args.summary_md.parent.is_dir():
        parser.error("summary_md parent directory ({}) does not exist".format(str(args.summary_md.parent)))

    log.info("Loading schema catalog metadata...")
    schema_catalog = SchemaCatalog(source=args.catalog_path_or_url)

    schema_json_by_name = {}
    for reference in schema_catalog.references:
        name = reference.name

        ref = None
        if isinstance(reference, GitSchemaReference):
            tags = sorted(reference.iter_tags(), key=by_commit_date, reverse=True)
            latest_tag = tags[0] if tags else None
            ref_names = itertools.chain(
                (ref.name for ref in tags),
                (ref.name for ref in reference.iter_branches()),
            )
            # Fallback to default branch name of repo if the refs specified by --ref option is not found.
            ref = args.ref if args.ref in ref_names else latest_tag
        schema_url = reference.get_schema_url(ref=ref)

        log.info("{}: downloading schema file from {}".format(name, schema_url))
        response = requests.get(schema_url)
        response.raise_for_status()
        schema_json = response.json()
        schema_json_by_name[name] = schema_json

        log.info("{}: generating Markdown file...".format(name))
        with (args.schemas / "{}.md".format(name)).open("w") as fd:
            readme_url = urljoin(schema_url, "README.md")
            response = requests.get(readme_url)
            if response.ok:
                log.info("{}: found README.md".format(name))
                readme_md = response.text
                fd.write(readme_md)
                fd.write("\n## Carte d'identité du schéma\n")
                fd.flush()
            log.info("{}: calling table-schema-to-markdown...".format(name))
            index_hbs = Path(__file__).parent / "index.hbs"
            subprocess.check_call(["table-schema-to-markdown", "--template", str(index_hbs), schema_url], stdout=fd)

        log.info("{}: generating XLSX template file...".format(name))
        subprocess.check_output(["table-schema-resource-template", "--format", "xlsx",
                                 "--created-date", "1111-11-11", "--modified-date", "1111-11-11",
                                 schema_url, str(args.templates / "{}.xlsx".format(name))])

    log.info("Generating SUMMARY.md for GitBook...")
    summary_template = Template(args.summary_j2.read_text())
    summary_md = summary_template.render(schemas=sorted([
        {
            "name": name,
            "title": schema_json.get("title", name)
        }
        for name, schema_json in schema_json_by_name.items()
    ], key=lambda v: strip_accents(v['title'].lower())))
    with args.summary_md.open("w") as fd:
        fd.write(summary_md)


def strip_accents(s):
    """Remove accents from string, used to sort normalized strings"""
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')


if __name__ == '__main__':
    sys.exit(main())
