#!/bin/bash

DATE=$(date "+%d/%m/%Y à %R")
sed -i -e "s~UPDATED_AT~${DATE}~" docs/README.md

gitbook build
gitbook pdf . "$PDF_FILE"  # needs "calibre" dependency
