#!/usr/bin/env python3

"""Setup deploy key and SSH_PRIVATE_KEY variable for each schema repository of SCDL."""

import argparse
import logging
import os
import sys
from pathlib import Path

import gitlab
from cryptography.hazmat.backends import default_backend as crypto_default_backend
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa

deploy_key_title = "Generate schema.md from schema.json"
scdl_group_name = "scdl"
schema_names = ["adresses", "catalogue", "deliberations",
                "equipements", "irve", "marches-publics", "prenoms", "subventions"]
ssh_key_email = "admin-validata@jailbreak.paris"
variable_name = "SSH_PRIVATE_KEY"
log = logging.getLogger(__name__)


def deploy_key_exists(schema_project, deploy_key_title):
    for deploy_key in schema_project.keys.list():
        if deploy_key.title == deploy_key_title:
            return True
    return False


def generate_ssh_key_pair():
    # From https://stackoverflow.com/a/39126754/3548266

    key = rsa.generate_private_key(
        backend=crypto_default_backend(),
        public_exponent=65537,
        key_size=2048
    )
    private_key = key.private_bytes(
        crypto_serialization.Encoding.PEM,
        crypto_serialization.PrivateFormat.PKCS8,
        crypto_serialization.NoEncryption()
    ).decode('utf-8')
    public_key = key.public_key().public_bytes(
        crypto_serialization.Encoding.OpenSSH,
        crypto_serialization.PublicFormat.OpenSSH
    ).decode('utf-8')
    return {"public_key": public_key, "private_key": private_key}


def get_variable_or_none(schema_project, variable_name):
    try:
        return schema_project.variables.get(variable_name)
    except gitlab.exceptions.GitlabGetError:
        return None


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--gitlab-instance-url', default='https://git.opendatafrance.net',
                        help='URL of GitLab instance')
    parser.add_argument('--private-token', default=os.getenv("GITLAB_API_PRIVATE_TOKEN"),
                        help='optional private token for authentication')
    parser.add_argument('--log', default='WARNING', help='level of logging messages')
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(message)s",
        level=numeric_level,
        stream=sys.stderr,  # script outputs data
    )

    gl = gitlab.Gitlab(args.gitlab_instance_url, private_token=args.private_token)

    for schema_name in schema_names:
        log.info("{}: find project...".format(schema_name))
        schema_project = gl.projects.get("{}/{}".format(scdl_group_name, schema_name))

        log.info("{}: generate SSH key pair...".format(schema_name))
        ssh_key_pair = generate_ssh_key_pair()

        variable = get_variable_or_none(schema_project, variable_name)
        if variable is not None:
            log.info("{}: project variable already exist, skipping".format(schema_name))
        else:
            log.info("{}: create project variable...".format(schema_name))
            variable = schema_project.variables.create({"key": variable_name, "value": ssh_key_pair["private_key"]})
            log.info("{}: variable created: {}".format(schema_name, variable))

        if deploy_key_exists(schema_project, deploy_key_title):
            log.info("{}: deploy key already exist, skipping".format(schema_name))
        else:
            log.info("{}: create project deploy key...".format(schema_name))
            deploy_key = schema_project.keys.create({
                'title': deploy_key_title,
                'key': ssh_key_pair["public_key"],
                'can_push': True,
            })
            log.info("{}: deploy key created: {}".format(schema_name, deploy_key))


if __name__ == '__main__':
    sys.exit(main())
