#!/bin/bash

CATALOG_URL="${CATALOG_URL:-https://git.opendatafrance.net/scdl/catalog/raw/master/catalog.json}"
CONTRIBUTING_MD_URL="${CONTRIBUTING_MD_URL:-https://git.opendatafrance.net/scdl/catalog/raw/master/CONTRIBUTING.md}"

wget -nc "$CONTRIBUTING_MD_URL" -O docs/CONTRIBUTING.md

mkdir -p docs/schemas docs/templates
python3 ./scripts/generate_files_from_schema_catalog.py "$CATALOG_URL" docs/schemas docs/templates docs_templates/SUMMARY.j2 docs/SUMMARY.md