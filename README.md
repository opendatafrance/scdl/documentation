# Documentation du Socle commun des données locales

Documentation portant sur les schémas du SCDL et autres informations complémentaires, publiée avec [Gitbook](https://github.com/GitbookIO/gitbook) à l'adresse https://scdl.opendatafrance.net/docs.

## Génération

La documentation sur [scdl.opendatafrance.net](https://scdl.opendatafrance.net/) est générée automatiquement tous les jours à partir du contenu listé ci-dessous. Si nécessaire, la génération peut également être déclenchée manuellement sur [la page Schedules](https://git.opendatafrance.net/scdl/documentation/pipeline_schedules) (permissions requises).

## Structure

Voici la liste des pages qui composent le site, et pour chacune la source de leur contenu :

### Contenu hébergé dans ce dépôt ([scdl/documentation](https://git.opendatafrance.net/scdl/documentation/))

- [Accueil](https://scdl.opendatafrance.net/docs/) : [/docs/README.md](/docs/README.md)
- [Recommandations relatives aux jeux de données](https://scdl.opendatafrance.net/docs/recommandations-relatives-aux-jeux-de-donnees.html) : [/docs/recommandations-relatives-aux-jeux-de-donnees.md](/docs/recommandations-relatives-aux-jeux-de-donnees.md)
- [Recommandations relatives aux schémas de validation](https://scdl.opendatafrance.net/docs/recommandations-relatives-aux-schemas-de-validation.html) : [/docs/recommandations-relatives-aux-schemas-de-validation.md](/docs/recommandations-relatives-aux-schemas-de-validation.md)
- [Badge Validata](https://scdl.opendatafrance.net/docs/badge.html) : [/docs/badge.md](/docs/badge.md)
- [Télécharger en PDF](https://scdl.opendatafrance.net/docs/t%C3%A9l%C3%A9charger.html) : [/docs/télécharger.md](/docs/t%C3%A9l%C3%A9charger.md)

### Contenu provenant d'autres dépôts

- La page de chaque schéma affiche successivement :
  - Le contenu du fichier README.md situé à la racine de son dépôt
  - Une "Carte d'identité du schéma" et un "Modèle de données" générés à partir du `schema.json`, grâce à [Table Schema to Markdown](https://www.npmjs.com/package/@opendataschema/table-schema-to-markdown)
- [Contribuer au SCDL](https://scdl.opendatafrance.net/docs/CONTRIBUTING.html) : fichier [CONTRIBUTING.md](https://git.opendatafrance.net/scdl/catalog/blob/master/CONTRIBUTING.md) situé dans [le dépôt du catalogue SCDL](https://git.opendatafrance.net/scdl/catalog/)

Par conséquent, une modification portant sur une des pages ci-dessus devra se faire à la source de celle-ci.

## Développement

Pour les développeurs, voici comment travailler sur la documentation en local sur sa machine.

Installer des outils nécessaires :

```bash
npm install -g gitbook-cli @opendataschema/table-schema-to-markdown
```

Installer un virtualenv Python contenant les dépendances :

```bash
mkvirtualenv scdl-documentation
pip install --requirement requirements.txt
```

Générer les fichiers non commités :

```bash
./scripts/generate_files.sh
```

Lancer le serveur gitbook :

```bash
gitbook serve
```

Ouvrir http://localhost:4000/

## Déploiement en production

La documentation est hébergée à l'adresse https://scdl.opendatafrance.net/docs sur un serveur chez Scaleway.

La [pipeline GitLab CI](.gitlab-ci.yml) de ce projet contient un job nommé `Deploy Gitbook` qui synchronise la documentation générée sur ce serveur.

### Gestion du certificat SSL

Le logiciel `certbot` est installé sur le serveur afin de gérer le certificat SSL. Son renouvellement est automatique grâce au "cron job" `/etc/cron.d/certbot`.

Les logs de l'exécution de certbot sont dans le fichier `/var/log/daemon.log` (chercher "certbot").

Pour renouveller manuellement le certificat, exécuter ceci en tant que `root` :

```bash
certbot renew
```
