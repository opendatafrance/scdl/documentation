# Télécharger en PDF

Cette documentation sur le Socle Commun des Données Locales peut être téléchargée au format PDF :

<a href="Socle%20Commun%20des%20Donn%C3%A9es%20Locales.pdf" target="_blank">Socle Commun des Données Locales.pdf</a>
